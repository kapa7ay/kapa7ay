// Example program
#include <iostream>
#include <string>
using namespace std;
class Nucleus{
    int nProtons, nNeutrons;
    public:
    void set(int p, int n){nProtons = p; nNeutrons = n;}
    double get_mass(){return (nProtons*938 + nNeutrons*939);}
};
int main()
{
    Nucleus pb208;
    pb208.set (82, 126);
    std::cout<<pb208.get_mass();
}