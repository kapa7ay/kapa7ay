#include "stdafx.h"
#include <iostream>
#include <vector>
#include <cmath>
#include <string>
using namespace std;
class Shape {
protected:
	int width, height;
public:
	Shape(int a = 0, int b = 0) {
		width = a;
		height = b;
	}
	int area() {                                                                //virtual int area () {
		cout << "Parent class area :" << endl;
		return 0;
	}
};

class Triangle : public Shape {
public:
	Triangle(int a = 0, int b = 0) :Shape (a, b) { }
	
	int area() {
		cout << "Triangle class area :" << endl;
		return (width*height/2);
	}
};

class Rectangle : public Shape {
public:
Rectangle(int a = 0, int b = 0) : Shape (a, b) { }
int area() {
	cout << "Rectangle class area :" << endl;
	return (width*height);
	}
};


int main()
{
	Shape * Ivelin;
	Triangle T(2, 3);
	Rectangle R(3, 4);
	Ivelin = &R;
	Ivelin->area();
    return 0;
}