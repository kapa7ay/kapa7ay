#include <iostream>
#include <string>
using namespace std;


string reverse_string(string input)
{
    for(int l=0, r=input.length()-1; l < r; l++, r--)
    {
        swap(input[l], input[r]);
    }
    return input;
}
int main()
{
    cout << reverse_string("ivelin");
}
