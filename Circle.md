// Example program
#include <iostream>
#include <string>
using namespace std;
class Circle {
    double radius;
    public:
    Circle(double r) { radius = r; }
    double circum() {
        return 2*radius*3.14159265;
    }
};
int main () {
    Circle foo (10.0);
    Circle bar = 20.0;
    Circle baz {30.0};
    Circle qux = {40.0};
    cout<<"foos circumference:" <<
    foo.circum () << '/n';
    return 0;
}